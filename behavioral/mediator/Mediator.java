package mediator;

interface AirTrafficControl {

    void requestLandingPermission(Airplane airplane);

    void notifyLandingComplete(Airplane airplane);

    void requestTakeoffPermission(Airplane airplane);

    void notifyTakeoffComplete(Airplane airplane);
}
