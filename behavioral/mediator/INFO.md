## Mediator Pattern


### INTENT

Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects 
from referring to each other explicitly, and it lets you vary their interaction independently.


### APPLICABILITY

- A set of objects communicate in well-defined but complex ways. The resulting interdependencies are unstructured and 
difficult to understand.
- Reusing an object is difficult because it refers to and communicates with many other objects.
- A behavior that's distributed between several classes should be customizable without a lot of subclassing.


### PARTICIPANTS

#### Mediator
- defines an interface to communicate with Colleague objects.

#### ConcreteMediator
- implements cooperative behavior by coordinating Colleague objects.
- knows and maintains its colleagues.

#### Colleague classes
- each Colleague class knows its Mediator object.
- each colleague communicates with its mediator whenever it would have otherwise communicated with another colleague.


### EXAMPLE SCENARIO: _AIRPLANE COORDINATION_

In this example, the control tower acts as a mediator for airplanes. The control tower will coordinate which airplanes 
can take off or land at any given time.  

In this example, the `ControlTower` acts as the mediator, coordinating the actions of different `Airplane` objects. The 
airplanes communicate their intentions to land or take off through the control tower, which manages the runway 
availability and queues the requests as needed.  

The Mediator pattern here simplifies the communication between airplanes and the control tower. Instead of each airplane 
needing to know the status of the runway or other airplanes, the control tower centralizes this logic, making it easier 
to manage and understand. This pattern is especially beneficial in complex systems where multiple objects need to 
coordinate their actions.


### CONSEQUENCES

#### It limits subclassing

A mediator localizes behavior that otherwise would be distributed among several objects. Changing this behavior requires 
subclassing Mediator only; Colleague classes can be reused as is.

#### It decouples colleagues

A mediator promotes loose coupling between colleagues. You can vary and reuse Colleague and Mediator classes 
independently.

#### It simplifies object protocols

A mediator replaces many-to-many interactions with one-to-many interactions between the mediator and its colleagues. 
One-to-many relationships are easier to understand, maintain, and extend.

#### It abstracts how objects cooperate

Making mediation an independent concept and encapsulating it in an object lets you focus on how objects interact apart 
from their individual behavior. That can help clarify how objects interact in a system.

#### It centralizes control

The Mediator pattern trades complexity of interaction for complexity in the mediator. Because a mediator encapsulates 
protocols, it can become more complex than any individual colleague. This can make the mediator itself a monolith that's 
hard to maintain.