package mediator;

public class Client {
    public static void main(String[] args) {
        AirTrafficControl controlTower = new ControlTower();

        Airplane flight1 = new Airplane("Flight 101", controlTower);
        Airplane flight2 = new Airplane("Flight 102", controlTower);

        flight1.requestLanding();
        flight2.requestLanding();
        flight2.requestTakeoff();
        flight2.requestTakeoff();
    }
}


