package mediator;

class Airplane {
    private final String callSign;
    private final AirTrafficControl controlTower;

    public Airplane(String callSign, AirTrafficControl controlTower) {
        this.callSign = callSign;
        this.controlTower = controlTower;
    }

    public String getCallSign() {
        return callSign;
    }

    public void requestLanding() {
        controlTower.requestLandingPermission(this);
    }

    public void land() {
        System.out.println(callSign + " is landing.");
        controlTower.notifyLandingComplete(this);
    }

    public void requestTakeoff() {
        controlTower.requestTakeoffPermission(this);
    }

    public void takeoff() {
        System.out.println(callSign + " is taking off.");
        controlTower.notifyTakeoffComplete(this);
    }
}
