package mediator;

import java.util.LinkedList;
import java.util.Queue;

class ControlTower implements AirTrafficControl {
    private final Queue<Airplane> landingQueue = new LinkedList<>();
    private final Queue<Airplane> takeoffQueue = new LinkedList<>();
    private boolean runwayAvailable = true;

    @Override
    public void requestLandingPermission(Airplane airplane) {
        if (runwayAvailable) {
            runwayAvailable = false;
            airplane.land();
        } else {
            System.out.println(airplane.getCallSign() + " waiting for landing.");
            landingQueue.add(airplane);
        }
    }

    @Override
    public void notifyLandingComplete(Airplane airplane) {
        System.out.println(airplane.getCallSign() + " has landed.");
        runwayAvailable = true;
        checkPendingAirplanes();
    }

    @Override
    public void requestTakeoffPermission(Airplane airplane) {
        if (runwayAvailable) {
            runwayAvailable = false;
            airplane.takeoff();
        } else {
            System.out.println(airplane.getCallSign() + " waiting for takeoff.");
            takeoffQueue.add(airplane);
        }
    }

    @Override
    public void notifyTakeoffComplete(Airplane airplane) {
        System.out.println(airplane.getCallSign() + " has taken off.");
        runwayAvailable = true;
        checkPendingAirplanes();
    }

    private void checkPendingAirplanes() {
        if (!landingQueue.isEmpty()) {
            landingQueue.poll().land();
        } else if (!takeoffQueue.isEmpty()) {
            takeoffQueue.poll().takeoff();
        }
    }
}
