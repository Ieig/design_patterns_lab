package iterator;

class BinaryTree implements Tree {

    private final TreeNode root;

    public BinaryTree(TreeNode root) {
        this.root = root;
    }

    @Override
    public Iterator createPreorderIterator() {
        return new PreorderIterator(root);
    }

    @Override
    public Iterator createInorderIterator() {
        return new InorderIterator(root);
    }

    @Override
    public Iterator createPostorderIterator() {
        return new PostorderIterator(root);
    }
}
