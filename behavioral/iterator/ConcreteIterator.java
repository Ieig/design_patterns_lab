package iterator;

import java.util.Deque;
import java.util.LinkedList;

class PreorderIterator implements Iterator {

    private final Deque<TreeNode> stack = new LinkedList<>();

    PreorderIterator(TreeNode root) {
        if (root != null) stack.push(root);
    }

    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public TreeNode next() {
        if (!hasNext()) return null;
        TreeNode node = stack.pop();
        if (node.right != null) stack.push(node.right);
        if (node.left != null) stack.push(node.left);
        return node;
    }
}

class InorderIterator implements Iterator {

    private final Deque<TreeNode> stack = new LinkedList<>();
    private TreeNode current;

    InorderIterator(TreeNode root) {
        current = root;
    }

    @Override
    public boolean hasNext() {
        return current != null || !stack.isEmpty();
    }

    @Override
    public TreeNode next() {
        while (current != null) {
            stack.push(current);
            current = current.left;
        }
        TreeNode node = stack.pop();
        current = node.right;
        return node;
    }
}

class PostorderIterator implements Iterator {
    private final Deque<TreeNode> stack = new LinkedList<>();
    private TreeNode lastVisitedNode = null;
    private TreeNode current;

    PostorderIterator(TreeNode root) {
        current = root;
    }

    @Override
    public boolean hasNext() {
        return current != null || !stack.isEmpty();
    }

    @Override
    public TreeNode next() {
        while (hasNext()) {
            while (current != null) {
                stack.push(current);
                current = current.left;
            }

            TreeNode node = stack.peek();
            if (node.right != null && lastVisitedNode != node.right) {
                current = node.right;
            } else {
                lastVisitedNode = stack.pop();
                return lastVisitedNode;
            }
        }
        return null;
    }
}

