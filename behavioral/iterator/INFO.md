## Iterator Pattern


### INTENT

Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.


### APPLICABILITY

- To access an aggregate object's contents without exposing its internal representation.
- To support multiple traversals of aggregate objects.
- To provide a uniform interface for traversing different aggregate structures (to support polymorphic iteration).


### PARTICIPANTS

#### Iterator
- defines an interface for accessing and traversing elements.

#### ConcreteIterator
- implements the Iterator interface.
- keeps track of the current position in the traversal of the aggregate.

#### Aggregate
- defines an interface for creating an Iterator object.

#### ConcreteAggregate
- implements the Iterator creation interface to return an instance of the proper ConcreteIterator.


### EXAMPLE SCENARIO: _BINARY TREE TRAVERSING_

Let's consider an example of the Iterator pattern with a tree structure, specifically a binary tree. We will implement 
concrete iterators for three types of depth-first traversals: preorder, inorder, and postorder.

In this example, `BinaryTree` creates different iterators for preorder, inorder, and postorder traversals. Each iterator 
follows the specific order of traversal for visiting the nodes of the tree. This demonstrates how the Iterator pattern 
can provide a uniform way to traverse different structures of a tree without exposing its internal representation.


### CONSEQUENCES

#### Supports variations in the traversal of an aggregate

Complex aggregates may be traversed in many ways. For example, code generation and semantic checking involve traversing 
parse trees. Code generation may traverse the parse tree inorder or preorder. Iterators make it easy to change the 
traversal algorithm: Just replace the iterator instance with a different one. You can also define Iterator subclasses to 
support new traversals.

#### Iterators simplify the Aggregate interface

Iterator's traversal interface obviates the need for a similar interface in Aggregate, thereby simplifying the 
aggregate's interface.

#### More than one traversal can be pending on an aggregate

An iterator keeps track of its own traversal state. Therefore, you can have more than one traversal in progress at once.