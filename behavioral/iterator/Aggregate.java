package iterator;

interface Tree {

    Iterator createPreorderIterator();

    Iterator createInorderIterator();

    Iterator createPostorderIterator();
}
