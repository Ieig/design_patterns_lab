package iterator;

interface Iterator {
    boolean hasNext();
    TreeNode next();
}
