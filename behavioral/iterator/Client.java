package iterator;

public class Client {
    public static void main(String[] args) {
        BinaryTree tree = getBinaryTree();

        System.out.println("Preorder traversal:");
        Iterator preorder = tree.createPreorderIterator();
        while (preorder.hasNext()) {
            System.out.println(preorder.next().value);
        }

        System.out.println("Inorder traversal:");
        Iterator inorder = tree.createInorderIterator();
        while (inorder.hasNext()) {
            System.out.println(inorder.next().value);
        }

        System.out.println("Postorder traversal:");
        Iterator postorder = tree.createPostorderIterator();
        while (postorder.hasNext()) {
            System.out.println(postorder.next().value);
        }
    }

    private static BinaryTree getBinaryTree() {
        TreeNode root = new TreeNode("3 root");
        TreeNode left = new TreeNode("1 left");
        TreeNode leftLeft = new TreeNode("0 left-left");
        TreeNode leftRight = new TreeNode("2 left-right");
        TreeNode right = new TreeNode("5 right");
        TreeNode rightLeft = new TreeNode("4 right-left");
        TreeNode rightRight = new TreeNode("6 right-right");

        root.setLeft(left);
        root.setRight(right);
        left.setLeft(leftLeft);
        left.setRight(leftRight);
        right.setLeft(rightLeft);
        right.setRight(rightRight);

        return new BinaryTree(root);
    }
}
