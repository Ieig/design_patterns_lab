package observer;

class CurrentConditionsDisplay implements Observer {

    @Override
    public void update(float temperature, float humidity, float pressure) {
        System.out.println("Current conditions: " + temperature + "F degrees and " + humidity + "% humidity");
    }
}

class ForecastDisplay implements Observer {

    @Override
    public void update(float temperature, float humidity, float pressure) {
        if (pressure > 1.0) {
            System.out.println("Weather is improving!");
        } else {
            System.out.println("Watch out for cooler, rainy weather");
        }
    }
}
