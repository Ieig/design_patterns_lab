package observer;

public class Client {
    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation();

        CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay();
        ForecastDisplay forecastDisplay = new ForecastDisplay();

        weatherStation.attach(currentDisplay);
        weatherStation.attach(forecastDisplay);

        weatherStation.setMeasurements(80, 65, 1.2f);
        weatherStation.setMeasurements(82, 70, 1.0f);
    }
}
