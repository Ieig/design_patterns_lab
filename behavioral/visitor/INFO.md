## Visitor Pattern


### INTENT

Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation 
without changing the classes of the elements on which it operates.


### APPLICABILITY

- An object structure contains many classes of objects with differing interfaces, and you want to perform operations on 
these objects that depend on their concrete classes.
- Many distinct and unrelated operations need tobe performed on objects in an object structure, and you want to avoid 
"polluting" their classes with these operations. Visitor lets you keep related operations together by defining them in 
one class. When the object structure is shared by many applications, use Visitor to put operations in just those 
applications that need them.
- The classes defining the object structure rarely change, but you often want to define new operations over the 
structure. Changing the object structure classes requires redefining the interface to all visitors, which is potentially
costly. If the object structure classes change often, then it's probably better to define the operations in those 
classes.


### PARTICIPANTS

#### Visitor
- declares a Visit operation for each class of ConcreteElement in the object structure. The operation's name and 
signature identifies the class that sends the Visit request to the visitor. That lets the visitor determine the 
concrete class of the element being visited. Then the visitor can access the element directly through its particular
interface.

#### ConcreteVisitor
- implements each operation declared by Visitor. Each operation implements a fragment of the algorithm defined for the 
corresponding class of object in the structure. ConcreteVisitor provides the context for the algorithm and stores its 
local state. This state often accumulates results during the traversal.

#### Element
- defines an Accept operation that takes a visitor as an argument.

#### ConcreteElement
- implements an Accept operation that takes a visitor as an argument.

#### ObjectStructure
- can enumerate its elements.
- may provide a high-level interface to allow the visitor to visit its elements.
- may either be a composite or a collection.


### EXAMPLE SCENARIO: __

In this example, we'll create a computer system composed of various parts (like `Computer`, `Mouse`, `Keyboard`, and 
`Monitor`). We'll then implement a visitor that can perform specific operations on each part.  

`Computer`, `Mouse`, `Keyboard`, and `Monitor` are concrete elements that implement the `ComputerPart` interface.
`ComputerPartVisitor` is an interface for visitors, and `ComputerPartDisplayVisitor` is a concrete visitor. Each 
element's `accept` method takes a visitor and calls the visitor's `visit` method. This way, the visitor can perform 
specific actions for each element type.  

The Visitor pattern allows adding new operations to existing object structures without modifying the structures. It's 
particularly useful when you have a complex object structure and want to perform various unrelated operations on these 
objects without changing their classes.


### CONSEQUENCES

#### Makes adding new operations easy

Visitors make it easy to add operations that depend on the components of complex objects. You can define a new operation 
over an object structure simply by adding a new visitor.In contrast, if you spread functionality over many classes, then 
you must change each class to define a new operation.

#### Gathers related operations and separates unrelated ones

Related behavior isn't spread over the classes defining the object structure; it's localized in a visitor. Unrelated 
sets of behavior are partitioned in their own visitor subclasses. That simplifies both the classes defining the elements 
and the algorithms defined in the visitors. Any algorithm-specific data structures can be hidden in the visitor.

#### Adding new ConcreteElement classes is hard

The Visitor pattern makes it hard to add new subclasses of Element. Each new ConcreteElement gives rise to a new 
abstract operation on Visitor and a corresponding implementation in every ConcreteVisitor class. Sometimes a default 
implementation can be provided in Visitor that can be inherited by most of the ConcreteVisitors, but this is the 
exception rather than the rule.  

So the key consideration in applying the Visitor pattern is whether you are mostly likely to change the algorithm 
applied over an object structure or the classes of objects that make up the structure. The Visitor class hierarchy
can be difficult to maintain when new ConcreteElement classes are added frequently. In such cases, it's probably easier 
just to define operations on the classes that make up the structure. If the Element class hierarchy is stable, but you 
are continually adding operations or changing algorithms, then the Visitor pattern will help you manage the changes.

#### Visiting across class hierarchies

An iterator can visit the objects in a structure as it traverses them by calling their operations. But an iterator can't 
work across object structures with different types of elements. Visitor does not have this restriction. It can visit 
objects that don't have a common parent class. You can add any type of object to a Visitor interface.

#### Accumulating state

Visitors can accumulate state as they visit each element in the object structure. Without a visitor, this state would be 
passed as extra arguments to the operations that perform the traversal, or they might appear as global variables.

#### Breaking encapsulation

Visitor's approach assumes that the ConcreteElement interface is powerful enough to let visitors do their job.As a 
result, the pattern often forces you to provide public operations that access an element's internal state, which may 
compromise its encapsulation.