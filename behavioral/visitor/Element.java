package visitor;

interface ComputerPart {

    void accept(ComputerPartVisitor computerPartVisitor);
}
