package strategy;

public class Client {
    public static void main(String[] args) {
        PaymentContext context = new PaymentContext();

        context.setPaymentStrategy(new CreditCardPayment("John Doe", "123456789"));
        context.executePayment(100);

        context.setPaymentStrategy(new PayPalPayment("john@example.com"));
        context.executePayment(200);

        context.setPaymentStrategy(new CryptoCurrencyPayment("1BoatSLRHtKNngkdXEeobR76b53LETtpyT"));
        context.executePayment(300);
    }
}
