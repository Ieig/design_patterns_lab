package strategy;

interface PaymentStrategy {
    void pay(int amount);
}
