## Memento Pattern


### INTENT

Without violation encapsulation, capture and externalize an object's internal state so that the object can be restored 
to this state later.


### APPLICABILITY

- A snapshot of (some portion of) an object's state must be saved so that it can be restored to that state later.
AND
- A direct interface to obtaining the state would expose implementation details and break the object's encapsulation.


### PARTICIPANTS

#### Memento
- stores internal state of the Originator object. The memento may store as much or as little of the originator's 
internal state as necessary at its originator's discretion.
- protects against access by objects other than the originator. Mementos have effectively two interfaces. Caretaker 
sees a narrow interface to the Memento—it can only pass the memento to other objects. Originator, in contrast, sees a 
wide interface, one that lets it access all the data necessary to restore itself to its previous state. Ideally, only 
the originator that produced the memento would be permitted to access the memento's internal state.

#### Originator
- creates a memento containing a snapshot of its current internal state.
- uses the memento to restore its internal state.

#### Caretaker
- is responsible for the memento's safekeeping.
- never operates on or examines the contents of a memento.


### EXAMPLE SCENARIO: _TEXT EDITOR WITH UNDO FUNCTIONALITY_

In this example, we'll create a simple text editor that allows undoing text changes.  

`TextEditor` acts as the Originator that creates mementos of its state and can restore its state from a memento.
`TextEditorMemento` is the Memento that holds the state of the `TextEditor`. Caretaker manages the mementos, providing 
undo and redo capabilities.


### CONSEQUENCES

#### Preserving encapsulation boundaries

Memento avoids exposing information that only an originator should manage but that must be stored nevertheless outside 
the originator. The pattern shields other objects from Originator internals, thereby preserving encapsulation boundaries.

#### Simplifying the Originator

In other encapsulation-preserving designs, Originator keeps the versions of internal state that clients have requested. 
That puts all the storage management burden on Originator. Having clients manage the state they ask for simplifies 
Originator and keeps clients from having to notify originators when they're done.

#### Using mementos might be expensive.

Mementos might incur considerable overhead if Originator must copy large amounts of information to store in the memento 
or if clients create and return mementos to the originator often enough. Unless encapsulating and restoring Originator 
state is cheap, the pattern might not be appropriate.

#### Defining narrow and wide interfaces.

It may be difficult in some languages to ensure that only the originator can access the memento's state.

#### Hidden costs in caring for mementos.

A caretaker is responsible for deleting the mementos it cares for. However, the caretaker has no idea how much state is 
in the memento. Hence, an otherwise lightweight caretaker might incur large storage costs when it stores mementos.