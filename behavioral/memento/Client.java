package memento;

public class Client {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        Caretaker caretaker = new Caretaker();

        editor.addText("Hello");
        caretaker.addMemento(editor.save());

        editor.addText(" World");
        caretaker.addMemento(editor.save());

        editor.addText(" Or not Hello");

        System.out.println("Current Text: " + editor.getText());

        // Undo
        TextEditorMemento memento0 = caretaker.latest();
        if (memento0 != null) {
            editor.restore(memento0);
            System.out.println("After Latest: " + editor.getText());
        }

        // Undo
        TextEditorMemento memento1 = caretaker.undo();
        if (memento1 != null) {
            editor.restore(memento1);
            System.out.println("After Undo: " + editor.getText());
        }

        // Redo
        TextEditorMemento memento2 = caretaker.redo();
        if (memento2 != null) {
            editor.restore(memento2);
            System.out.println("After Redo: " + editor.getText());
        }
    }
}
