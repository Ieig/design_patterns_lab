package memento;

import java.util.ArrayList;
import java.util.List;

class Caretaker {

    private List<TextEditorMemento> mementos = new ArrayList<>();
    private int currentMementoIndex = -1;

    public void addMemento(TextEditorMemento memento) {
        // Remove any "future" mementos after the current index before adding a new one
        if (currentMementoIndex != mementos.size() - 1) {
            mementos = mementos.subList(0, currentMementoIndex + 1);
        }
        mementos.add(memento);
        currentMementoIndex++;
    }

    public TextEditorMemento undo() {
        if (currentMementoIndex <= 0) return null;
        return mementos.get(--currentMementoIndex);
    }

    public TextEditorMemento latest() {
        if (currentMementoIndex < 0) return null;
        return mementos.get(currentMementoIndex);
    }

    public TextEditorMemento redo() {
        if (currentMementoIndex >= mementos.size() - 1) return null;
        return mementos.get(++currentMementoIndex);
    }
}
