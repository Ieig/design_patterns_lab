package memento;

class TextEditor {

    private StringBuilder currentText;

    public TextEditor() {
        currentText = new StringBuilder();
    }

    public void addText(String text) {
        currentText.append(text);
    }

    public String getText() {
        return currentText.toString();
    }

    public TextEditorMemento save() {
        return new TextEditorMemento(currentText.toString());
    }

    public void restore(TextEditorMemento memento) {
        currentText = new StringBuilder(memento.getState());
    }
}
