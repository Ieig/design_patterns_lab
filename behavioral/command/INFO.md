## Command Pattern


### INTENT

Encapsulate request as an object, thereby letting you parameterize clients with different requests, queue or log 
requests, and support undoable operations.


### APPLICABILITY

- Parametrize objects by an action to perform. Commands are an object-oriented replacements for callbacks.
- Specify, queue, and execute requests at different times. A Command object may have a lifetime independent of the 
original request. If the receiver of a request can be represented in an address space-independent way, then you can 
transfer a command object for the request to a different process and fulfill the request there.
- Support undo. The Command's Execute operation can store state for reversing its effects in the command itself. The 
Command interface must have an added Unexecute operation that reverses the effects of a previous call to Execute. 
Executed commands are stored in a history list. Unlimited-level undo and redo is achieved by traversing this list 
backwards and forwards calling Unexecute and Execute, respectively.
- Support logging changes so that they can be reapplied in case of a system crash. By augmenting the Command interface 
with load and store operations, you can keep a persistent log of changes. Recovering from a crash involves reloading 
logged commands from disk and reexecuting them with the Execute operation.
- Structure a system around high-level operations built on primitives operations. Such a structure is common in 
information systems that support transactions. A transaction encapsulates a set of changes to data. The Command pattern 
offers a way to model transactions. Commands have a common interface, letting you invoke all transactions the same way. 
The pattern also makes it easy to extend the system with new transactions.


### PARTICIPANTS

#### Command
- declares an interface for executing an operation.

#### ConcreteCommand
- defines a binding between a Receiver object and an action.
- implements Execute by invoking the corresponding operation(s) on Receiver.

#### Client
- creates a ConcreteCommand object and sets its receiver.

#### Invoker
- asks the command to carry out the request.

#### Receiver
- knows how to perform the operations associated with carrying out a request. Any class may serve as a Receiver.


### EXAMPLE SCENARIO: _REMOTE CONTROL SYSTEM_

Let's consider a home automation example where a remote control can be configured to control various devices like 
lights and fans.  

In this example, `RemoteControl` acts as an invoker, `LightOnCommand`, `LightOffCommand`, `FanOnCommand` and 
`FanOffCommand` are concrete commands, `Light` and `Fan` are receivers, and `Command` is the command interface. The 
Command pattern encapsulates a request as an object, thereby allowing for parameterization of clients with different 
requests, queuing of requests, and logging of requests. It also provides the capability to support undoable operations.  

The possibility to execute more than one commands in transactional manner is present in the example. This behavior is 
achieved by the use of a `RemoteControl` extension `TransactionalRemoteControl`, which can take a list of `Command` 
objects, execute them one by one and in case of an exception rollback all the changes it has made.


### CONSEQUENCES

#### Decoupling

The Invoker is decoupled from the Receiver. This means changes to the Receiver's implementation don't affect the Invoker.

#### Undo/Redo Capability

The pattern allows implementing undo/redo functionality. Each command knows how to undo its action.

#### Reusability and Composability

Commands can be reused and composed. Can create macro commands that execute multiple commands in sequence.

#### Extensibility

It's easy to add new commands without changing existing code, adhering to the Open/Closed Principle.

#### Flexibility in Execution

Commands can be queued, logged, or delayed.