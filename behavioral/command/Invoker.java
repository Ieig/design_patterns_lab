package command;

import java.util.List;

class RemoteControl {

    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }

    public void pressUndo() {
        command.undo();
    }
}

class TransactionalRemoteControl extends RemoteControl {

    public void executeCommands(List<Command> commands) {
        int comNum = 0;

        for (int i = 0; i < commands.size(); i++) {
            try {
                commands.get(i).execute();
            } catch (Exception e) {
                comNum = i;
                break;
            }
        }

        if (comNum != 0) {
            for (int i = 0; i < comNum; i++) {
                try {
                    commands.get(i).undo();
                } catch (Exception ignored) {}
            }
        }
    }
}
