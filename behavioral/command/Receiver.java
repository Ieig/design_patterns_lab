package command;

class Light {

    public void turnOn() {
        System.out.println("Light is on");
    }

    public void turnOff() {
        System.out.println("Light is off");
    }
}

class Fan {

    public void turnOn() {
        System.out.println("Fan is on");
    }

    public void turnOff() {
        System.out.println("Fan is off");
    }
}