package command;

import java.util.List;

public class Client {
    public static void main(String[] args) {
        // Receivers
        Light light = new Light();
        Fan fan = new Fan();

        // Commands
        Command lightOn = new LightOnCommand(light);
        Command lightOff = new LightOffCommand(light);
        Command fanOn = new FanOnCommand(fan);
        Command fanOff = new FanOffCommand(fan);

        // Invoker
        RemoteControl simpleRemote = new RemoteControl();
        TransactionalRemoteControl advancedRemote = new TransactionalRemoteControl();

        // Simple command use
        simpleRemote.setCommand(lightOn);
        simpleRemote.pressButton();

        simpleRemote.pressUndo();

        // Transactional macro command use
        advancedRemote.executeCommands(List.of(new LightOnCommand(light), new FanOnCommand(fan)));
    }
}
