package interpreter;

public class Client {
    public static void main(String[] args) {
        String input = "x y + z -";

        Context context = new Context();
        context.setVariable("x", 5);
        context.setVariable("y", 3);
        context.setVariable("z", 2);

        Expression expression = ExpressionParser.parseExpression(input);

        int result = expression.interpret(context);
        System.out.println(input + " = " + result);
    }
}
