## Interpreter Pattern


### INTENT

Given a language, define a representation for its grammar along with an interpreter that uses the representation to 
interpret sentences in the language.


### APPLICABILITY

The Interpreter pattern is good when there is a language to interpret, and you can represent statements in the language 
as abstract syntax trees. The Interpreter pattern works best when:
- the grammar is simple. For complex grammars, the class hierarchy for the grammar becomes large and unmanageable. Tools 
such as parser generators are a better alternative in such cases. They can interpret expressions without building 
abstract syntax trees, which can save space and possibly time.
- efficiency is not a critical concern. The most efficient interpreters are usually not implemented by interpreting 
parse trees directly but by first translating them into another form. For example, regular expressions are often 
transformed into state machines. But even then, the translator can be implemented by the Interpreter pattern, so the 
pattern is still applicable.


### PARTICIPANTS

#### AbstractExpression
- declares an interface for executing an operation.

#### TerminalExpression
- implements the Expression interface for terminal symbols in the grammar.
- an instance is required for every terminal symbol in a sentence.

#### NonterminalExpression
- implements the Expression interface for nonterminal symbols in the grammar and stores the child Expression instances.
- represents these kinds of symbols (or rules) in the grammar that are not terminal symbols. Terminal symbols are the 
most basic units (like numbers, operators, etc.), while nonterminal expressions are more complex and made up of one or 
more terminal or nonterminal expressions.
- maintains instance variables of type AbstractExpression for each of the symbol it can be broken down to.
- interpreting typically calls itself recursively on the variables it encloses (symbols).

#### Context
- contains information that's global to the interpreter. In many implementations, particularly simple ones, the context 
might be implicit or not used at all.

#### Client
- builds (or is given) an abstract syntax tree (AST) representing a particular sentence in the language that the grammar 
defines. The abstract syntax tree is assembled from instances of the NonterminalExpression and TerminalExpression 
classes.
- invokes the Interpret operation.


### EXAMPLE SCENARIO: _ARITHMETIC EXPRESSION INTERPRETER_

We'll build an interpreter for binary arithmetic expressions. This will involve parsing and evaluating the expression.  

`NumberExpression` represents the terminal expressions (numbers). `AdditionExpression` and `SubtractionExpression` are 
nonterminal expressions. The `ExpressionParser` class parses the input string and constructs an AST from `Expression` 
objects. The `Client` class demonstrates how to parse and interpret an arithmetic expression.  

This interpreter can evaluate expressions written in postfix notation (also known as Reverse Polish Notation), where the 
operator follows the operands. For instance, "5 3 + 2 -" is interpreted as "(5 + 3) - 2".  

The Interpreter pattern provides a way to evaluate language grammars or expressions. It's particularly useful for simple 
languages, domain-specific languages, or for parsing expressions where the complexity doesn't justify using a more 
sophisticated parsing technique.  

In this scenario, we can introduce a `Context` class that holds additional information necessary for interpreting 
expressions, like variable values or an operation history. The `Context` class is introduced to hold variable values. 
Expressions can now include variables (like "x", "y" and "z"), and their values are fetched from the Context during 
interpretation. This modification makes the interpreter more versatile, allowing it to handle expressions with variable 
names whose values are determined at runtime.


### CONSEQUENCES

#### Easy to change and extend the grammar

Because the pattern uses classes to represent grammar rules, you can use inheritance to change or extend the grammar. 
Existing expressions can be modified incrementally, and new expressions can be defined as variations on old ones.

#### Implementing the grammar is easy

Classes defining nodes in the abstract syntax tree have similar implementations. These classes are easy to write, and
often their generation can be automated with a compiler or parser generator.

#### Complex grammars are hard to maintain

The Interpreter pattern defines at least one class for every rule in the grammar (grammar rules defined using BNF may 
require multiple classes). Hence, grammars containing many rules can be hard to manage and maintain. Other design 
patterns can be applied to mitigate the problem. But when the grammar is very complex, other techniques such as parser 
or compiler generators are more appropriate.

#### Adding new ways to interpret expressions

The Interpreter pattern makes it easier to evaluate an expression in a new way. For example, you can support pretty 
printing or type-checking an expression by defining a new operation on the expression classes. If you keep creating 
new ways of interpreting an expression, then consider using the Visitor pattern to avoid changing the grammar classes.