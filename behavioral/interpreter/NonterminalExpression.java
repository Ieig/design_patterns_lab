package interpreter;

class AdditionExpression implements Expression {

    private final Expression firstExpression;
    private final Expression secondExpression;

    public AdditionExpression(Expression firstExpression, Expression secondExpression) {
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
    }

    @Override
    public int interpret(Context context) {
        return firstExpression.interpret(context) + secondExpression.interpret(context);
    }
}

class SubtractionExpression implements Expression {

    private final Expression firstExpression;
    private final Expression secondExpression;

    public SubtractionExpression(Expression firstExpression, Expression secondExpression) {
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
    }

    @Override
    public int interpret(Context context) {
        return firstExpression.interpret(context) - secondExpression.interpret(context);
    }
}
