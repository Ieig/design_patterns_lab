package interpreter;

class NumberExpression implements Expression {
    private final String number;

    public NumberExpression(String number) {
        this.number = number;
    }

    @Override
    public int interpret(Context context) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return context.getVariable(number); // Get value from context
        }
    }
}
