package interpreter;

import java.util.Deque;
import java.util.LinkedList;

public class ExpressionParser {

    static Expression parseExpression(String input) {
        Deque<Expression> stack = new LinkedList<>();
        Expression leftExpression;
        Expression rightExpression;

        for (String token : input.split(" ")) {
            switch (token) {
                case "+":
                    rightExpression = stack.pop();
                    leftExpression = stack.pop();
                    Expression addition = new AdditionExpression(leftExpression, rightExpression);
                    stack.push(addition);
                    break;
                case "-":
                    rightExpression = stack.pop();
                    leftExpression = stack.pop();
                    Expression subtraction = new SubtractionExpression(leftExpression, rightExpression);
                    stack.push(subtraction);
                    break;
                default:
                    Expression numberExpression = new NumberExpression(token);
                    stack.push(numberExpression);
            }
        }

        return stack.pop();
    }
}
