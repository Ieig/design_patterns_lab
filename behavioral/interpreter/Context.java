package interpreter;

import java.util.HashMap;
import java.util.Map;

class Context {

    private final Map<String, Integer> variableValues;

    public Context() {
        variableValues = new HashMap<>();
    }

    public void setVariable(String variableName, int value) {
        variableValues.put(variableName, value);
    }

    public int getVariable(String variableName) {
        return variableValues.getOrDefault(variableName, 0);
    }
}
