package state;

public class Client {
    public static void main(String[] args) {
        TrafficLight trafficLight = new TrafficLight();

        for (int i = 0; i < 10; i++) {
            System.out.println(trafficLight.getStatus());
            trafficLight.change();
        }
    }
}
