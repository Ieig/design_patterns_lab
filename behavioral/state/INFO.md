## State Pattern


### INTENT

Allow an object to alter its behavior when its internal state changes. The object will appear to change its state.

### APPLICABILITY

- An object's behavior depends on its state, and it must change its behavior at run-time depending on that state.
- Operations have large, multipart conditional statements that depend on the object's state. This state is usually
represented by one or more enumerated constants. Often, several operations will contain this same conditional structure. 
The State pattern puts each branch of the conditional in a separate class. This lets you treat the object's state as an 
object in its own right that can vary independently of other objects.


### PARTICIPANTS

#### Context
- defines an interface of interest to clients.
- maintains an instance of a ConcreteState subclass that defines the current state.

#### State
- defines an interface for encapsulating the behavior associated with a particular state of the Context.

#### ConcreteState subclasses
- each subclass implements a behavior associated with a state of the Context.


### EXAMPLE SCENARIO: _TRAFFIC LIGHT SYSTEM_

In this example, we'll create a traffic light system where the traffic light changes state between green, yellow, and 
red.  

In this example, the `TrafficLight` acts as the context. It has states represented by `GreenState`, `YellowState`, and 
`RedState`. The traffic light changes its state from green to yellow, then to red, and back to green through yellow, 
cycling through the states. Each state knows which state comes next, making the transition logic straightforward and 
encapsulated within the state classes.  

The State pattern is powerful for managing state transitions and behaviors associated with different states, providing 
a cleaner solution than using a large number of conditional statements.


### CONSEQUENCES

#### Localization of state-specific behavior and partitions behavior for different states

The State pattern puts all behavior associated with a particular state into one object. Because allstate-specific code 
lives in a State subclass, new states and transitions can be added easily by defining new subclasses.  

An alternative is to use data values to define internal states and have Context operations check the data explicitly. 
But then we'd have look-alike conditional or case statements scattered throughout Context's implementation. Adding a 
new state could require changing several operations, which complicates maintenance.  

The State pattern avoids this problem but might introduce another, because the pattern distributes behavior for 
different states across several State subclasses. This increases the number of classes and is less compact than a 
single class. But such distribution is actually good if there are many states, which would otherwise necessitate 
large conditional statements.  

Like long procedures, large conditional statements are undesirable. They're monolithic and tend to make the code 
less explicit, which in turn makes them difficult to modify and extend. The State pattern offers a better way to 
structure state-specific code. The logic that determines the state transitions doesn't reside in monolithic if or 
switch statements but instead is partitioned between the State subclasses. Encapsulating each state transition and
action in a class elevates the idea of an execution state to full object status. That imposes structure on the code 
and makes its intent clearer.

#### Makes state transitions explicit and atomic

When an object defines its current state solely in terms of internal data values, its state transitions have no 
explicit representation; they only show up as assignments to some variables. Introducing separate objects for 
different states makes the transitions more explicit.

Also, State objects can protect the Context from inconsistent internal states, because state transitions are atomic 
from the Context's perspective—they happen by rebinding one variable (the Context's State object variable), not
several.

#### State objects can be shared

If State objects have no instance variables—that is, the state they represent is encoded entirely in their type—then 
contexts can share a State object. When states are shared in this way, they are essentially flyweights with no intrinsic 
state, only behavior