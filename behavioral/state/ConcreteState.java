package state;

class GreenState implements TrafficLightState {

    @Override
    public void change(TrafficLight trafficLight) {
        trafficLight.setState(new YellowState());
    }

    @Override
    public String getStatus() {
        return "Green Light";
    }
}

class YellowState implements TrafficLightState {

    @Override
    public void change(TrafficLight trafficLight) {
        if (trafficLight.isGoingToGreen()) {
            trafficLight.setState(new GreenState());
        } else {
            trafficLight.setState(new RedState());
        }
    }

    @Override
    public String getStatus() {
        return "Yellow Light";
    }
}

class RedState implements TrafficLightState {

    @Override
    public void change(TrafficLight trafficLight) {
        trafficLight.setState(new YellowState());
    }

    @Override
    public String getStatus() {
        return "Red Light";
    }
}
