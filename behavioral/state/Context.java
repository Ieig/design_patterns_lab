package state;

class TrafficLight {

    private TrafficLightState state;
    private boolean goingToGreen;

    public TrafficLight() {
        this.state = new GreenState();
    }

    public void setState(TrafficLightState state) {
        this.state = state;
        if (state instanceof GreenState) {
            goingToGreen = false;
        } else if (state instanceof RedState) {
            goingToGreen = true;
        }
    }

    public void change() {
        state.change(this);
    }

    public String getStatus() {
        return state.getStatus();
    }

    public boolean isGoingToGreen() {
        return goingToGreen;
    }
}
