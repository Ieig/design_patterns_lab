package state;

interface TrafficLightState {

    void change(TrafficLight trafficLight);

    String getStatus();
}
