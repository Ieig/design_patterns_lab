package chainofresponsibility;

abstract class DocumentHandler {

    protected DocumentHandler nextHandler;

    public DocumentHandler(DocumentHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public abstract void handleDocument(String documentType);
}

