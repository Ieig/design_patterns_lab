package chainofresponsibility;

public class Client {
    public static void main(String[] args) {
        DocumentHandler management = new ManagementDocumentHandler(null);
        DocumentHandler financial = new FinancialDocumentHandler(management);
        DocumentHandler legal = new LegalDocumentHandler(financial);

        legal.handleDocument("Legal");
        legal.handleDocument("Financial");
    }
}

