package chainofresponsibility;

class LegalDocumentHandler extends DocumentHandler {

    public LegalDocumentHandler(DocumentHandler nextHandler) {
        super(nextHandler);
    }

    public void handleDocument(String documentType) {
        if (documentType.equals("Legal")) {
            System.out.println("Legal department has approved the document.");
        }
        if (nextHandler != null) {
            nextHandler.handleDocument(documentType);
        }
    }
}

class FinancialDocumentHandler extends DocumentHandler {

    public FinancialDocumentHandler(DocumentHandler nextHandler) {
        super(nextHandler);
    }

    public void handleDocument(String documentType) {
        if (documentType.equals("Financial")) {
            System.out.println("Financial department has approved the document.");
        }
        if (nextHandler != null) {
            nextHandler.handleDocument(documentType);
        }
    }
}

class ManagementDocumentHandler extends DocumentHandler {

    public ManagementDocumentHandler(DocumentHandler nextHandler) {
        super(nextHandler);
    }

    public void handleDocument(String documentType) {
        System.out.println("Management has approved the document.");
    }
}

