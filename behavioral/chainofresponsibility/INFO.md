## Chain of Responsibility Pattern


### INTENT

Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. 
Chain the receiving and pass the request along the chain until an object handles it.


### APPLICABILITY

- More than one object may handle the request, and the handler isn't known a priori. The handler should be ascertained 
automatically.
- Want to issue a request to one of several objects without specifying the receiver explicitly.
- The set of objects that can handle the request should be specified dynamically.


### PARTICIPANTS

#### Handler
- defines an interface for handling requests.
- implements the successor link (optional).

#### ConcreteHandler
- handles requests it is responsible for.
- can access its successor.
- if the ConcreteHandler can handle the request, it does so; otherwise it forwards the request to its successor.

#### Client
- initiates the request to a ConcreteHandler object on the chain.


### EXAMPLE SCENARIO: _DOCUMENT APPROVAL PROCESS_

Let's take an example of a document approval process in an organization. The document needs to be approved by different 
departments (e.g., Legal, Financial, Management) before it becomes official. We'll use the Chain of Responsibility 
pattern to handle these approvals.  

In this example, each handler processes the document if it falls under its responsibility; otherwise, it passes the 
document to the next handler in the chain. The `LegalDocumentHandler` handles legal documents, the
`FinancialDocumentHandler` handles financial documents, and the `ManagementDocumentHandler` is the final approver in 
the chain.


### CONSEQUENCES

#### Reduced coupling

The pattern frees an object from knowing which other object handles a request. An object only has to know that a request 
will be handled appropriately. Both the receiver and the handler have no explicit knowledge of each other, and an object 
in the chain doesn't have to know about the chain structure.

#### Simplification of Object Interconnections

Chain of Responsibility can simplify object interconnections. Instead of objects maintaining references to all candidate 
receivers, they keep a single reference to their successor.

#### Added flexibility in assigning responsibilities

Chain or Responsibility giver you added flexibility in distributing responsibilities among objects. You can add or 
change responsibilities for handling a request by adding to or otherwise changing the chain at run-time. You can combine 
this with subclassing to specialize handlers statically.

#### Receipt isn't guaranteed

Since a request has no explicit receiver, there's no guarantee it'll be handled—the request can fall off the end of 
the chain without ever being handled. A request can also go unhandled when the chain is not configured properly.

#### Potential Performance Issues

The pattern can potentially impact performance, especially when a request travels through a long chain before it is 
handled.