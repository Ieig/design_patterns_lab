package templatemethod;

public class Client {
    public static void main(String[] args) {
        Meal pasta = new PastaMeal();
        Meal salad = new SaladMeal();

        System.out.println("Preparing pasta:");
        pasta.prepareMeal();

        System.out.println("\nPreparing salad:");
        salad.prepareMeal();
    }
}
