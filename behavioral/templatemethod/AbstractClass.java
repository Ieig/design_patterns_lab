package templatemethod;

abstract class Meal {
    // Template method
    public final void prepareMeal() {
        prepareIngredients();
        cook();
        serve();
    }

    // Abstract steps to be implemented by subclasses
    abstract void prepareIngredients();
    abstract void cook();

    // Common step that's the same for all subclasses (hook operations)
    void serve() {
        System.out.println("The meal is served.");
    }
}
