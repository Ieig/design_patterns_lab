## Template Method Pattern


### INTENT

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template Method lets subclasses 
redefine certain steps of an algorithm without changing the algorithm's structure.


### APPLICABILITY

- To implement the invariant parts of an algorithm once and leave it up to subclasses to implement the behavior that 
can vary.
- When common behavior among subclasses should be factored and localized in a common class to avoid code duplication. 
This is a good example of "refactoring to generalize" as described by Opdyke and Johnson. You first identify the 
differences in the existing code and then separate the differences into new operations. Finally, you replace the 
differing code with a template method that calls one of these new operations.
- To control subclasses extensions. You can define a template method that calls "hook" operations at specific points, 
thereby permitting extensions only at those points.


### PARTICIPANTS

#### AbstractClass
- acts as a blueprint for the algorithm's structure. It lays out the steps of the algorithm, some of which are not fully 
defined and are left for subclasses to implement.

**Abstract Primitive Operations:** These are the steps in the algorithm that are intentionally left undefined (or 
"abstract") in the AbstractClass. They are like placeholders that need to be filled in by subclasses.

**Template Method:** A method in the AbstractClass that outlines the algorithm's structure. It calls both the abstract 
primitive operations (which subclasses will define) and may also call concrete operations (already defined in the 
AbstractClass or other objects). The key point is that the overall sequence of steps in the algorithm is defined here, 
but the specific details of some steps are deferred to subclasses.

#### ConcreteClass
- inherits from AbstractClass and implements the specific details of the abstract primitive operations. In other words, 
it fills in the blanks left by AbstractClass.

**Implementing the Steps:** The ConcreteClass provides the concrete behavior for the primitive operations that were only 
abstractly defined in the AbstractClass. This way, different ConcreteClass subclasses can implement these steps in 
different ways, but the overarching algorithm structure remains unchanged.


### EXAMPLE SCENARIO: _MEAN PREPARATION_

We'll design a simple scenario of a meal preparation process in a restaurant, where the steps of preparing a meal are 
defined, but the specific details vary depending on the type of meal.  

In this example, `Meal` is the abstract class with a template method `prepareMeal` and abstract methods 
`prepareIngredients` and `cook`. `PastaMeal` and `SaladMeal` are concrete classes that implement these abstract methods.  

The template method ensures that the overall structure of the meal preparation process is the same, but the specific 
details vary based on the meal being prepared. The Template Method pattern is particularly useful for scenarios like 
this, where the high-level structure of a process is common across different implementations, but the details of some 
steps vary. This pattern allows for defining a "template" for the process and deferring the exact details of some steps 
to subclasses.


### CONSEQUENCES

Template methods are a fundamental technique for code reuse. They are particularly important in class libraries, because 
they are the means for factoring out common behavior in library classes.  

Template methods lead to an inverted control structure that's sometimes referred to as "the Hollywood principle," that 
is, "Don't call us, we'll call you". This refers to how a parent class calls the operations of a subclass and not the 
other way around.  

Template methods call the following kinds of operations:
- concrete operations (either on the ConcreteClass or on client classes);
- concrete AbstractClass operations (i.e., operations that are generally useful to subclasses);
- primitive operations (i.e., abstract operations);
- factory methods;
- hook operations, which provide default behavior that subclasses can extend if necessary. A hook operation often does 
nothing by default.

It's important for template methods to specify which operations are hooks (may be overridden) and which are abstract 
operations (must be overridden). To reuse an abstract class effectively, subclass writers must understand which 
operations are designed for overriding.