package templatemethod;

class PastaMeal extends Meal {

    @Override
    void prepareIngredients() {
        System.out.println("Gathering ingredients for pasta.");
    }

    @Override
    void cook() {
        System.out.println("Cooking pasta.");
    }
}

class SaladMeal extends Meal {

    @Override
    void prepareIngredients() {
        System.out.println("Gathering ingredients for salad.");
    }

    @Override
    void cook() {
        System.out.println("Mixing the salad.");
    }
}
