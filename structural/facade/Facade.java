package facade;

class HomeTheaterFacade {

    private final TV tv;
    private final SoundSystem soundSystem;
    private final BluRayPlayer bluRay;

    public HomeTheaterFacade(TV tv, SoundSystem soundSystem, BluRayPlayer bluRay) {
        this.tv = tv;
        this.soundSystem = soundSystem;
        this.bluRay = bluRay;
    }

    public void watchMovie() {
        System.out.println("Get ready to watch a movie...");
        tv.turnOn();
        soundSystem.turnOn();
        soundSystem.setVolume(5);
        bluRay.turnOn();
        bluRay.play();
    }

    public void endMovie() {
        System.out.println("Shutting down the home theater...");
        bluRay.turnOff();
        soundSystem.turnOff();
        tv.turnOff();
    }
}

