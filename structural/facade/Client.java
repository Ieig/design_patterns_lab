package facade;

public class Client {
    public static void main(String[] args) {
        TV tv = new TV();
        SoundSystem soundSystem = new SoundSystem();
        BluRayPlayer bluRay = new BluRayPlayer();

        HomeTheaterFacade homeTheater = new HomeTheaterFacade(tv, soundSystem, bluRay);
        homeTheater.watchMovie();
        homeTheater.endMovie();
    }
}
