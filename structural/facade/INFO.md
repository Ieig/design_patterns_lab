## Facade Pattern


### INTENT

Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes 
the subsystem easier to use.


### APPLICABILITY

- Want to provide a simple interface for a complex subsystem. Subsystems often get more complex as they evolve. Most 
patterns, when applied, result in more and smaller classes. This makes the subsystem more reusable and easier to 
customize, but it also becomes harder to use for clients that don't need to customize it. A facade can provide a simple 
default view of the subsystem that is good enough for most clients. Only clients needing more customizability will need 
to look beyond this facade.
- There are many dependencies between clients and the implementation classes of an abstraction. Introduce a facade to
decouple the subsystem from clients and other subsystems, thereby promoting subsystem independence and portability.
- Want to layer your subsystems. Use a facade to define an entry point to each subsystem level. If subsystems are 
dependant, then you can simplify the dependencies between them by making them communicate with each other solely through 
their facades.


### PARTICIPANTS

#### Facade
- knows which subsystem classes are responsible for a request.
- delegates client requests to appropriate subsystem objects.

#### subsystem classes (Subsystem)
- implement subsystem functionality.
- handle work assigned by the Facade object.
- have no knowledge of the facade; that is, they keep no reference to it.


### EXAMPLE SCENARIO: _HOME THEATRE SYSTEM_

Let's create a scenario where we have a home theater system composed of multiple components like a TV, a sound system, 
and a Blu-ray player. We'll use the Facade pattern to simplify the operation of this complex system.  

In this example, the `HomeTheaterFacade` provides a simple interface (`watchMovie` and `endMovie`) to the complex 
subsystems (`TV`, `SoundSystem`, `BluRayPlayer`). This makes it easier for the client to use the home theater system 
without needing to understand the intricacies of how the components interact with each other. The Facade pattern 
effectively decouples the client from the complex subsystem.


### CONSEQUENCES

#### Covering subsystem components

It shields clients from subsystem components, thereby reducing the number of objects that clients deal with and making 
the subsystem easier to use.

#### Promoting weak coupling

It promotes weak coupling between the subsystem and its clients. Often the components in a subsystem are strongly 
coupled. Weak coupling lets you vary the components of the subsystem without affecting its clients. Facades help 
layer a system and the dependencies between objects. They can eliminate complex or circular dependencies. This can 
be an important consequence when the client and the subsystem are implemented independently.  

Reducing compilation dependencies is vital in large software systems. You want to save time by minimizing recompilation 
when subsystem classes change. Reducing compilation dependencies with facades can limit the recompilation needed for a 
small change in an important subsystem. A facade can also simplify porting systems to other platforms, because it's less 
likely that building one subsystem requires building all others.

#### Optional in use

It doesn't prevent applications from using subsystem classes if they need to. Thus, you can choose between ease of use 
and generality.