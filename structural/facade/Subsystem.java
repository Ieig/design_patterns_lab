package facade;

class TV {

    public void turnOn() {
        System.out.println("TV turned on");
    }

    public void turnOff() {
        System.out.println("TV turned off");
    }
}

class SoundSystem {

    public void turnOn() {
        System.out.println("Sound system turned on");
    }

    public void turnOff() {
        System.out.println("Sound system turned off");
    }

    public void setVolume(int level) {
        System.out.println("Sound system volume set to " + level);
    }
}

class BluRayPlayer {

    public void turnOn() {
        System.out.println("Blu-ray player turned on");
    }

    public void turnOff() {
        System.out.println("Blu-ray player turned off");
    }

    public void play() {
        System.out.println("Blu-ray movie playing");
    }
}

