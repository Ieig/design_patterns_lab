## Proxy Pattern


### INTENT

Provide a surrogate or placeholder for another object to control access to it.


### APPLICABILITY

Proxy is applicable whenever there is a need for a more versatile or sophisticated reference to an object than a simple 
pointer. Here are several common situations in which the Proxy pattern is applicable:
- A remote proxy provides a local representative for an object in a different address space.
- A virtual proxy creates expensive objects on demand.
- A protection proxy controls access to the original object. Protection proxies are useful when objects should have 
different access rights.


### PARTICIPANTS

#### Proxy
- maintains a reference that lets the proxy access the real subject. Proxy may refer to a Subject if the RealSubject and 
Subject interfaces are the same.
- provides an interface identical to Subject's so that a proxy can be substituted for the real subject.
- controls access to the real subject and may be responsible for creating and deleting it.

#### Subject
- defines the common interface for RealSubject and Proxy so that a Proxy can be used anywhere a RealSubject is expected.

#### RealSubject
- defines the real object that the proxy represents.


### EXAMPLE SCENARIO: _IMAGE LOADING_

In this example, we'll use the Proxy pattern to create a virtual proxy for image loading. This is a common use case in 
applications where large images need to be loaded and displayed. The proxy will control access to the image object by 
loading it only when it's actually needed (lazy loading).  

In this example, `ProxyImage` controls access to `RealImage`. It defers the loading of the image file until it's 
actually needed (when `display()` is called). This can significantly enhance performance when dealing with large images 
or a large number of image objects.


### CONSEQUENCES

#### Separation of Concerns:
The proxy can handle tasks like lazy initialization, logging, or access control, while the real object focuses on its 
core functionality.

#### Improved Performance: 
Especially in the case of virtual proxies, it can lead to performance improvements, as it avoids loading large objects 
until they are actually needed.

#### Access Control: 
Proxies can control the access to an object, which is useful in scenarios where security or certain usage policies need 
to be enforced.

#### Increased Complexity: 
Introducing proxies can add complexity to the code, as there is an additional layer between the client and the actual 
object.

#### Potential Delay in Object Access: 
In scenarios where the proxy performs significant processing (like network requests or extensive initialization), it can 
introduce a delay in accessing the actual object.