package proxy;

public class Client {
    public static void main(String[] args) {
        Image image = new ProxyImage("test_image.jpg");

        image.display();
    }
}

