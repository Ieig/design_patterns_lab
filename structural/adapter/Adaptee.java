package adapter;

interface ThirdPartyLogger {

    void sendLogMessage(String message);
}

class SpecificLogger implements ThirdPartyLogger {

    @Override
    public void sendLogMessage(String message) {
        System.out.println("Specific third-party logger: " + message);
    }
}
