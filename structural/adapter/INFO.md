## Pattern


### INTENT

Convert the interface of a class into another interface clients expect. Adapter lets classes work together that 
couldn't otherwise because of incompatible interfaces.


### APPLICABILITY

- Want to use an existing class, and its interface does not match the one you need.
- Want to create a reusable class that cooperates with unrelated or unforeseen classes, that is, classes that don't
necessarily have compatible interfaces.
- Want to use several existing classes, but it's impractical to adapt their interface by
subclassing every one. An object adapter can adapt the interface of its parents (object adapter only).


### PARTICIPANTS

#### Target
- defines the domain-specific interface that Client uses.

#### Client
- collaborates with objects conforming to the Target.

#### Adaptee
- defines an existing interface (class) that need adapting.

#### Adapter
- adapts the interface of Adaptee to the Target.

### EXAMPLE SCENARIO: _LOGGING_

Imagine a scenario in a logging framework where you have a Target interface `Logger` and an existing third-party logging
framework with its own interface, `ThirdPartyLogger` as an Adaptee. In this example, `LoggerClassAdapter` and 
`LoggerObjectAdapter` adapt `ThirdPartyLogger` to the `Logger` interface, but use different ways to do it: the first by 
multiple inheritance and the second by composition. The client can use any implementation of `ThirdPartyLogger` via the
`Logger` interface, promoting flexibility and reducing the coupling between the client's code and the third-party 
logging framework.


### CONSEQUENCES

#### _CLASS ADAPTER_

- Adapts Adaptee to Target by committing to a concrete Adaptee class. As a consequence, a class adapter won't work when 
we want to adapt a class and all its subclasses.
- Lets Adapter override some of Adaptee's behavior, since Adapter is a subclass of Adaptee.
- Introduces only one object, and no additional pointer indirection is needed to get to the adaptee.

#### _OBJECT ADAPTER_

- Lets a single Adapter work with many Adaptees - that is, the Adaptee itself and all of its subclasses (if any). The 
Adapter can also add functionality to all Adaptees at once.
- Makes it harder to override Adaptee behavior. It will require subclassing Adaptee and making Adapter refer to the 
subclass rather than the Adaptee itself.

#### Adapting amounts

Adapters vary in the amount of work they do to adapt Adaptee to the Target interface. There is a spectrum of possible
work, from simple interface conversion — like, changing the names of operations—to supporting an entirely different 
set of operations. The amount of work Adapter does, depends on how similar the Target interface is to Adaptee's.

#### Two-way adapters

A potential problem with adapters is that they aren't transparent to all clients. An adapted object no longer conforms 
to the Adaptee interface, so it can't be used as is wherever an Adaptee object can. Two-way adapters can provide such 
transparency. Specifically, they're useful when two different clients need to view an object differently.


### IMPORTANT NOTES

#### _A class adapter uses multiple inheritance to adapt one interface to another_
#### _An object adapter relist on object composition_