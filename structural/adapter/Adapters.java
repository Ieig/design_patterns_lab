package adapter;

class LoggerClassAdapter implements Logger, ThirdPartyLogger {

    @Override
    public void sendLogMessage(String message) {
        System.out.println("Specific logger behavior: " + message);
    }

    @Override
    public void log(String message) {
        sendLogMessage(message);
    }
}

class LoggerObjectAdapter implements Logger {

    private final ThirdPartyLogger thirdPartyLogger;

    public LoggerObjectAdapter(ThirdPartyLogger thirdPartyLogger) {
        this.thirdPartyLogger = thirdPartyLogger;
    }

    @Override
    public void log(String message) {
        thirdPartyLogger.sendLogMessage(message);
    }
}
