package adapter;

interface Logger {

    void log(String message);
}
