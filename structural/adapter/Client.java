package adapter;

public class Client {
    public static void main(String[] args) {
        Logger logger = new LoggerClassAdapter();
        logger.log("This is a log message for class adapter.");

        ThirdPartyLogger thirdPartyLogger = new SpecificLogger();
        logger = new LoggerObjectAdapter(thirdPartyLogger);

        logger.log("This is a log message object adapter.");
    }
}
