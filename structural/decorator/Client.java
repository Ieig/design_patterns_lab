package decorator;

public class Client {
    public static void main(String[] args) {
        Coffee simpleCoffee = new SimpleCoffee();
        System.out.println(simpleCoffee.getDescription() + " Cost: $" + simpleCoffee.cost());

        Coffee coffeeWithMilk = new WithMilk(simpleCoffee);
        System.out.println(coffeeWithMilk.getDescription() + " Cost: $" + coffeeWithMilk.cost());

        Coffee coffeeWithMilkAndSugar = new WithSugar(coffeeWithMilk);
        System.out.println(coffeeWithMilkAndSugar.getDescription() + " Cost: $" + coffeeWithMilkAndSugar.cost());

        Coffee coffeeWithDoubleMilkAndSugar = new WithMilk(coffeeWithMilkAndSugar);
        System.out.println(coffeeWithDoubleMilkAndSugar.getDescription() + " Cost: $" + coffeeWithDoubleMilkAndSugar.cost());
    }
}
