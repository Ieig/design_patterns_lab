## Decorator Pattern


### INTENT

Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to a subclassing 
for extending functionality.


### APPLICABILITY

- To add responsibilities to individual objects dynamically and transparently, that is, without affecting other objects.
- For responsibilities that can be withdrawn.
- When extension by subclassing is impractical. Sometimes a large number of independent extensions are possible and 
would produce an explosion of subclasses to support every combination. Or a class definition maybe hidden or otherwise 
unavailable for subclassing.


### PARTICIPANTS

#### Component
- defines an interface for objects that can have responsibilities added to them dynamically.

#### ConcreteComponent
- defines an object to which additional responsibilities can be attached.

#### Decorator
- maintains a reference to a Component object and defines an interface that conforms to Component's interface.

#### ConcreteDecorator
- adds responsibilities to the component.


### EXAMPLE SCENARIO: _COFFEE CUSTOMIZATION_

In this example, we'll consider a coffee shop scenario where we can dynamically add extras (like milk, sugar) to basic 
coffee types.  

In this example, `SimpleCoffee` is the base component. `WithMilk` and `WithSugar` are decorators that add additional 
behavior to the coffee. We can stack multiple decorators on top of one another, each adding its functionality, thus 
demonstrating the flexibility of the Decorator pattern.  

**Dynamic Composition:** With the Decorator pattern, you can dynamically add or remove functionality to/from objects at 
runtime. In contrast, a direct implementation like `CoffeeWithMilk` and `CoffeeWithSugar` is static and doesn't offer 
this level of flexibility. For instance, if you want a coffee with double sugar and milk, you can easily create it with 
decorators without defining a new class.  

**Avoid Class Explosion:** If you had to create a new class for every possible combination of coffee and extras, you 
would end up with a large number of classes (e.g., `SimpleCoffee`, `CoffeeWithMilk`, `CoffeeWithSugar`, 
`CoffeeWithMilkAndSugar`, `CoffeeWithDoubleMilk`, etc.). The Decorator pattern allows you to combine functionalities 
freely without creating a new class for each combination.  

**Open/Closed Principal:** The Decorator pattern adheres to the Open/Closed Principle. Your system is open for extension 
(you can add new decorators) but closed for modification (no need to change existing classes or interfaces). Adding a 
new ingredient doesn't require modifying the existing coffee classes or interfaces.

**Single Responsibility Principle:** Each decorator class has a single responsibility. `WithMilk` only adds milk, 
`WithSugar` only adds sugar. This makes your code easier to maintain and extend.

**Selective Inheritance:** Decorators can selectively add or override the behavior of the wrapped object without 
affecting other objects of the same class.


### CONSEQUENCES

#### More flexibility than static inheritance

The Decorator pattern provides a more flexible way to add responsibilities to objects than can be had with static 
(multiple) inheritance. With decorators, responsibilities can be added and removed at run-time simply by attaching and 
detaching them. In contrast, inheritance requires creating a new class for each additional responsibility. This gives 
rise to many classes and increases the complexity of a system. Furthermore, providing different Decorator classes for a 
specific Component class lets you mix and match responsibilities.

#### Avoids feature-laden classes high up in the hierarchy

Decorator offers a pay-as-you-go approach to adding responsibilities. Instead of trying to support all foreseeable 
features in a complex, customizable class, you can define a simple class and add functionality incrementally with 
Decorator objects. Functionality can be composed of simple pieces. As a result, an application needn't pay for features 
it doesn't use. It's also easy to define new kinds of Decorators independently of the classes of objects they extend, 
even for unforeseen extensions. Extending a complex class tends to expose details unrelated to the responsibilities 
you're adding.

#### A decorator and its component aren't identical

A decorator acts as a transparent enclosure. But from an object identity point of view, a decorated component is not 
identical to the component itself. Hence, you shouldn't rely on object identity when you use decorators.

#### Lots of little objects

A design that uses Decorator often results in systems composed of lots of little objects that all look alike. The 
objects differ only in the way they are interconnected, not in their class or in the value of their variables. Although 
these systems are easy to customize by those who understand them, they can be hard to learn and debug.