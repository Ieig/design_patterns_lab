package decorator;

interface Coffee {

    String getDescription();

    double cost();
}
