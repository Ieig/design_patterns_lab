package composite;

class Circle implements Graphic {

    public void draw() {
        System.out.println("Drawing a circle");
    }

    public void add(Graphic graphic) {
        // Not applicable for Leaf
    }

    public void remove(Graphic graphic) {
        // Not applicable for Leaf
    }

    public Graphic getChild(int index) {
        // Not applicable for Leaf
        return null;
    }
}

class Rectangle implements Graphic {

    public void draw() {
        System.out.println("Drawing a rectangle");
    }

    @Override
    public void add(Graphic graphic) {
        // Not applicable for Leaf
    }

    @Override
    public void remove(Graphic graphic) {
        // Not applicable for Leaf
    }

    @Override
    public Graphic getChild(int index) {
        return null;
    }
}

