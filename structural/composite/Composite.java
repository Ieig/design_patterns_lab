package composite;

import java.util.ArrayList;
import java.util.List;

class CompositeGraphic implements Graphic {

    private final List<Graphic> childGraphics = new ArrayList<>();

    public void draw() {
        for (Graphic graphic : childGraphics) {
            graphic.draw();
        }
    }

    public void add(Graphic graphic) {
        childGraphics.add(graphic);
    }

    public void remove(Graphic graphic) {
        childGraphics.remove(graphic);
    }

    public Graphic getChild(int index) {
        return childGraphics.get(index);
    }
}

