## Composite Pattern


### INTENT

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual 
objects and compositions of objects uniformly.


### APPLICABILITY

- Want to represent part-whole hierarchies of objects.
- Want client to ignore the difference between composition of objects and individual objects. Clients will treat all 
objects in the composite structure uniformly.


### PARTICIPANTS

#### Component
- declares the interface for objects in the composition.
- implements default behavior for the interface common to all classes, as appropriate.
- declares an interface for accessing and managing its child components.
- defines ann interface for accessing a component's parent in the recursive structure, and implements it if that's 
appropriate (optional).

#### Leaf
- represents leaf objects in the composition. A leaf has no children.
- defines behavior for primitive objects in the composition.

#### Composite
- defines behavior for components having children.
- stores child components.
- implements child-related operations in the Component interface.

#### Client
- manipulates objects in the composition through the Component interface.


### EXAMPLE SCENARIO: _GRAPHIC OBJECTS_

In this example, let's create a scenario where graphic objects (like circles, rectangles) and groups of graphic objects 
can be treated the same way. We'll have a common interface for both individual graphic objects and their compositions.  

In this example, `Circle` and `Rectangle` are leaf objects, and `CompositeGraphic` is a composite object that can 
contain any number of `Graphic` objects (`Circle`, `Rectangle`, or other composites). This setup allows us to call the 
`draw` method on individual objects or on compositions of objects seamlessly, demonstrating the essence of the 
Composite pattern.


### CONSEQUENCES

#### Defines the class hierarchies consisting of primitive objects and composite objects

Primitive objects can be composed into more complex objects, which in turn can be composed, and so on recursively. 
Wherever client code expects a primitive object, it can also take a composite object.

#### Makes the client simple

Clients can treat composite structures and individual objects uniformly. Clients normally don't know (and shouldn't care)
whether they're dealing with a leaf or a composite component. This simplifies client code, because it avoids having to 
write tag-and-case-statement-style functions over the classes that define the composition.

#### Makes it easier to add new kinds of components

Newly defined Composite or Leaf subclasses work automatically with existing structures and client code. Clients don't 
have to be changed for new Component classes.

#### Can make design overly general

The disadvantage of making it easy to add new components is that it makes it harder to restrict the components of a 
composite. Sometimes you want a composite to have only certain components. With Composite, you can't rely on the type 
system to enforce those constraints for you. You'll have to use run-time checks instead.