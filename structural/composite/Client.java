package composite;

public class Client {
    public static void main(String[] args) {
        Graphic circle1 = new Circle();
        Graphic circle2 = new Circle();
        Graphic rectangle = new Rectangle();

        CompositeGraphic graphic = new CompositeGraphic();
        graphic.add(circle1);
        graphic.add(circle2);
        graphic.add(rectangle);

        CompositeGraphic graphicGroup = new CompositeGraphic();
        graphicGroup.add(graphic);
        graphicGroup.draw();
    }
}
