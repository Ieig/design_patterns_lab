package flyweight;

import java.util.HashMap;
import java.util.Map;

class TreeTypeFactory {

    private static final Map<String, TreeType> treeTypes = new HashMap<>();

    public static TreeType getTreeType(String type, String texture) {
        TreeType treeType = treeTypes.get(type);

        if (treeType == null) {
            treeType = new ConcreteTreeType(type, texture);
            treeTypes.put(type, treeType);
        }
        return treeType;
    }
}

