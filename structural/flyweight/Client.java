package flyweight;

public class Client {
    public static void main(String[] args) {
        Forest forest = new Forest("Italy", "Summer");

        forest.plantTree(1, 2, 5, 3, "Oak", "Green Texture");
        forest.plantTree(5, 3, 4, 2, "Pine", "Dark Texture");
        forest.plantTree(1, 2, 5, 3, "Oak", "Green Texture");

        forest.draw();
    }
}