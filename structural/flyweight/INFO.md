## Flyweight Pattern


### INTENT

Use sharing to support large numbers of fine-grained objects efficiently.


### APPLICABILITY

#### To achieve effectiveness in using Flyweight need all of those to be true:

- An application uses a large number of objects.
- Storage costs are high because of the sheer quantity of objects.
- Most object state can be made extrinsic.
- Many groups of objects can be replaced by relatively few shared objects once extrinsic state is removed.
- The application does not depend on object identity. Since flyweight objects may be shared, identity test will return 
true for conceptually distinct objects.


### PARTICIPANTS

#### Flyweight
- declares an interface through which flyweights can receive and act on extrinsic state.

#### ConcreteFlyweight
- implements Flyweight interface and adds storage for intrinsic state, if any. A ConcreteFlyweight object must be 
sharable. Any state it stores must be intrinsic; that is, must be independent of the ConcreteFlyweight object's context.

#### UnsharedConcreteFlyweight
- not all Flyweight subclasses need to be shared. The Flyweight interface enables sharing; it doesn't enforce it. It's 
common for UnsharedConcreteFlyweight objects to have ConcreteFlyweight objects as children at some level in the 
flyweight object structure.

#### FlyweightFactory
- creates and manages flyweight objects.
- ensures that flyweights are shared properly. When a client requests a flyweight, the FlyweightFactory object supplies 
an existing instance or creates one if none exists.

#### Client
- maintains reference to flyweight(s).
- computes or stores the extrinsic state of flyweight(s).


### EXAMPLE SCENARIO: _FOREST RENDERING_

In this example, trees in a forest are rendered. Each tree has some shared properties (like a type and texture) and some 
unique properties (like position, height, and width). The shared properties are handled by Flyweights, while the unique 
properties are handled by UnsharedConcreteFlyweights.  

In this complex example, `TreeTypeFactory` creates and manages `ConcreteTreeType` instances (the shared state, like a 
type and texture). Each `Tree` object (the UnsharedConcreteFlyweight) has its own unique state (position, height, and 
width) but shares the `TreeType` object. This approach efficiently manages memory usage in the application, which is 
critical for rendering a large number of trees in the forest. The shared state is centralized, while the unique state 
is maintained in individual Tree instances.


### CONSEQUENCES

Flyweights may introduce run-time costs associated with transferring, finding, and/or computing extrinsic state, 
especially if it was formerly stored as an intrinsic state. However, such costs are offset by space savings, which
increase as more flyweights are shared.  

#### Storage savings are functional of several factors
- the reduction in the total number of instances that comes from sharing
- the amount of intrinsic state per object
- whether extrinsic state is computed or stored.

The more flyweights are shared, the greater the storage savings. The savings increase with the amount of shared state. 
The greatest savings occur when the objects use substantial quantities of both intrinsic and extrinsic state, and the
extrinsic state can be computed rather than stored. Then you save on storage in two ways: Sharing reduces the cost of 
intrinsic state, and you trade extrinsic state for computation time.  

The Flyweight pattern is often combined with the **Composite** pattern to represent a hierarchical structure as a graph 
with shared leaf nodes. A consequence of sharing is that flyweight leaf nodes cannot store a pointer to their parent. 
Rather, the parent pointer is passed to the flyweight as part of its extrinsic state. This has a major impact on how 
the objects in the hierarchy communicate with each other