package flyweight;

import java.util.ArrayList;
import java.util.List;

class Tree {

    private final double x;
    private final double y;
    private final double height;
    private final double width;
    private final TreeType type;

    public Tree(double x, double y, double height, double width, TreeType type) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.type = type;
    }

    public void draw() {
        System.out.println("Drawing a tree at position (" + x + ", " + y + ") with height: "
                + height + " and width: " + width);
        type.draw();
    }
}

class Forest {

    private final List<Tree> trees;
    private final String region;
    private final String season;

    public Forest(String region, String season) {
        this.trees = new ArrayList<>();
        this.region = region;
        this.season = season;
    }

    public void plantTree(double x, double y, double height, double width, String type, String texture) {
        TreeType treeType = TreeTypeFactory.getTreeType(type, texture);
        Tree tree = new Tree(x, y, height, width, treeType);
        trees.add(tree);
    }

    public void draw() {
        System.out.println("Drawing forest in " + region + " during the " + season + " season:");
        for (Tree tree : trees) {
            tree.draw();
        }
    }
}

