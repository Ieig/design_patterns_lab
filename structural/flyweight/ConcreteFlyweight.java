package flyweight;

class ConcreteTreeType implements TreeType {

    private final String type;
    private final String texture;

    public ConcreteTreeType(String type, String texture) {
        this.type = type;
        this.texture = texture;
    }

    @Override
    public void draw() {
        System.out.println("Drawing tree of type: " + type + " and texture: " + texture);
    }
}

