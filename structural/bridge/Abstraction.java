package bridge;

abstract class RemoteControl {

    protected Device device;

    protected RemoteControl(Device device) {
        this.device = device;
    }

    public abstract void togglePower();
    public abstract void nextChannel();
    public abstract void previousChannel();
}
