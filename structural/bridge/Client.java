package bridge;

public class Client {
    public static void main(String[] args) {
        Device tv = new TV();
        RemoteControl simpleRemote = new SimpleRemoteControl(tv);
        simpleRemote.togglePower();
        simpleRemote.nextChannel();

        Device radio = new Radio();
        RemoteControl advancedRemote = new AdvancedRemoteControl(radio);
        advancedRemote.togglePower();
        advancedRemote.nextChannel();
    }
}
