package bridge;

class SimpleRemoteControl extends RemoteControl {

    private int currentChannel = 1;

    public SimpleRemoteControl(Device device) {
        super(device);
    }

    public void togglePower() {
        System.out.println("Simple remote: power toggle");
        device.turnOn();
    }

    public void nextChannel() {
        device.setChannel(++currentChannel);
    }

    public void previousChannel() {
        device.setChannel(--currentChannel);
    }
}

class AdvancedRemoteControl extends RemoteControl {

    private int currentChannel = 1;

    public AdvancedRemoteControl(Device device) {
        super(device);
    }

    public void togglePower() {
        System.out.println("Advanced remote: power toggle");
        device.turnOn();
    }

    public void nextChannel() {
        device.setChannel(++currentChannel);
    }

    public void previousChannel() {
        device.setChannel(--currentChannel);
    }
}
