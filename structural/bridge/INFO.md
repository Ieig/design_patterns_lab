## Bridge Pattern

### INTENT

Decouple an abstraction from its implementation so that the two can vary independently.


### APPLICABILITY

- Want to avoid a permanent binding between an abstraction and its implementation. This might be the case, for example, 
when the implementation must be selected or switched at run-time.
- Both the abstractions and their implementations should be extensible by subclassing. In this case, the Bridge pattern 
lets you combine the different abstractions and implementations and extend them independently.
- Changes in the implementation of an abstraction should have no impact on clients; that is, their code should not have 
to be recompiled.
- Have a proliferation of classes. Such hierarchy indicates the need for splitting an object into two parts. Rumbaugh 
uses the term "nested generalizations" to refer to such class hierarchies.
- Want to share an implementation among multiple objects and this fact should be hidden from the client.


### PARTICIPANTS

#### Abstraction
- defines the abstraction's interface.
- maintains a reference to an object of type Implementor.

#### RefinedAbstraction
- extends the interface defined by Abstraction.

#### Implementor
- defines the interface for implementation classes. This interface doesn't have to correspond exactly to Abstraction's 
interface; in fact the two interfaces can be quite different. Typically, the Implementor interface provides only 
primitive operations, and Abstraction defines higher-level operations based on these primitives.

#### ConcreteImplementor
- implements the Implementor interface and defines its concrete implementation.


### EXAMPLE SCENARIO: _DEVICES AND REMOTE CONTROLS_

We'll create a scenario involving different devices and their remote controls. The aim is to separate the control 
(abstraction) from the device being controlled (implementation), allowing each to vary independently.  

In this example, `RemoteControl` is the abstraction, and `Device` is the implementation. `SimpleRemoteControl` and
`AdvancedRemoteControl` are refined abstractions, while `TV` and `Radio` are concrete implementors. The Bridge pattern 
allows us to use different remote controls for different devices, demonstrating how the control mechanism (remote 
control) and the device being controlled can vary independently.


### CONSEQUENCES

#### Decoupling interface and implementation

An implementation is not bound permanently to an interface. The implementation of an abstraction can be configured at 
run-time. Its even possible for an object to change its implementation at run-time.

Decoupling Abstraction and Implementor also eliminates compile-time dependencies on the implementation. Changing an 
implementation class doesn't require recompiling the Abstraction class and its clients. This property is essential 
when you must ensure binary compatibility between different versions of a class library.  

Furthermore, this decoupling encourages layering that can lead to a better structured system. The high-level part of 
a system only has to know about Abstraction and Implementor.

#### Improved extensibility

You can extend the Abstraction and Implementor hierarchies independently.

#### Hiding implementation details from clients

You can shield clients from implementation details, like the sharing of implementor objects and the accompanying 
reference count mechanism.