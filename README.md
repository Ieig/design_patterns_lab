## Creational Patterns:

> _These patterns deal with object creation mechanisms, trying to create objects in a manner suitable to the situation._
> _The basic form of object creation could result in design problems or added complexity to the design._
> _Creational design patterns solve this problem by somehow controlling this object creation._

<br/>

- ### [Abstract Factory][af]
- ### [Builder][bl]
- ### [Factory Method][fm]
- ### [Prototype][pt]
- ### [Singleton][sn]

<br/>

## Structural Patterns:

> _These patterns are concerned with how classes and objects are composed to form larger structures._
> _Structural class creation patterns use inheritance to compose interfaces._
> _Structural object patterns define ways to compose objects to realize new functionality._

<br/>

- ### [Adapter][ad]
- ### [Bridge][br]
- ### [Composite][cs]
- ### [Decorator][dc]
- ### [Facade][fc]
- ### [Flyweight][fw]
- ### [Proxy][px]

<br/>

## Behavioral Patterns:

> _These patterns are concerned with algorithms and the assignment of responsibilities between objects._
> _Behavioral patterns describe not just patterns of objects or classes but also the patterns of communication between them._

<br/>

- ### [Chain of Responsibility][cr]
- ### [Command][cd]
- ### [Interpreter][in]
- ### [Iterator][it]
- ### [Mediator][md]
- ### [Memento][mm]
- ### [Observer][ob]
- ### [State][st]
- ### [Strategy][sr]
- ### [Template Method][tm]
- ### [Visitor][vs]

[//]: #
 [fm]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/creational/factorymethod?ref_type=heads>
 [af]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/creational/abstractfactory?ref_type=heads>
 [sn]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/creational/singleton?ref_type=heads>
 [pt]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/creational/prototype?ref_type=heads>
 [bl]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/creational/builder?ref_type=heads>
 [ad]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/adapter?ref_type=heads>
 [br]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/bridge?ref_type=heads>
 [cs]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/composite?ref_type=heads>
 [dc]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/decorator?ref_type=heads>
 [fc]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/facade?ref_type=heads>
 [fw]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/flyweight?ref_type=heads>
 [px]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/structural/proxy?ref_type=heads>
 [cr]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/chainofresponsibility?ref_type=heads>
 [cd]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/command?ref_type=heads>
 [in]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/interpreter?ref_type=heads>
 [it]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/iterator?ref_type=heads>
 [md]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/mediator?ref_type=heads>
 [mm]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/memento?ref_type=heads>
 [ob]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/observer?ref_type=heads>
 [st]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/state?ref_type=heads>
 [sr]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/strategy?ref_type=heads>
 [tm]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/templatemethod?ref_type=heads>
 [vs]: <https://gitlab.com/Ieig/design_patterns_lab/-/tree/main/behavioral/visitor?ref_type=heads>