## Singleton Pattern


### INTENT

Ensure a class only has one instance, and provide a global point of access to it.


### APPLICABILITY

- There must be exactly one instance of a class, and it must be accessible to clients from a well-known access point.
- When the sole instance should be extensible by subclassing, and clients should be able to use an extended instance 
without modifying their code.


### PARTICIPANTS

#### Singleton
- Defines an Instance operation that lets clients access its unique instance. Instance is a class operation.
- May be responsible for creating its own unique instance


### EXAMPLE SCENARIO:

- `DoubleCheckLockSingleton` is lazy, has volatile field, double check for null equality with sync inside.
- `EarlyInitSingleton` is not lazy, created right away, simple one.
- `InitOnDemandSingleton` is lazy, using nested static class for lazy init.
- Enum based Singleton and the simplest of all.


### CONSEQUENCES

#### Controlled Access
It provides controlled access to the sole instance. Since the Singleton class encapsulates its sole instance, 
it can have strict control over how and when clients access it.

#### Lazy Initialization
Singleton allows for lazy initialization, where the instance is created only when it is needed for the first time.

#### Namespace Organization / Reduced Global State
The Singleton pattern is an improvement over global variables. It avoids polluting the name space with global variables 
that store sole instances.

#### Shared Resources/Common State
Singleton is useful when exactly one instance is needed to coordinate actions across the system, 
like a database connection or a file manager.