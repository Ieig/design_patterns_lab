## Factory Method Pattern


### INTENT

Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a 
class defer instantiation to subclasses.


### APPLICABILITY

- a class can't anticipate the class of objects it must create.
- a class wants its subclasses to specify the objects it creates.
- classes delegate responsibility to one of several helper subclasses, and you want to localize the knowledge of which 
helper subclass is the delegate.


### PARTICIPANTS

#### Product
- defines the interface of objects the factory method creates.

#### ConcreteProduct
- implements the Product interface.

#### Creator
- declares the factory method, which returns an object of type Product. Creator may also define a default implementation 
of the factory method that returns a default ConcreteProduct object.
- may call the factory method to create a Product object.

#### ConcreteCreator
- overrides the factory method to return an instance of a ConcreteProduct.


### EXAMPLE SCENARIO: _LOGISTICS SYSTEM_

Consider a scenario of a logistics system, where different types of transport are used for delivery.

- `Transport` interface defines the `deliver` method.
- `Truck` and `Ship` are concrete implementations of the `Transport` interface.
- `Logistics` contains a `createTransport` factory method that returns a `Transport` object.
- `RoadLogistics` and `SeaLogistics` are subclasses of `Logistics`. Each overrides `createTransport` to return a 
different type of `Transport` object.


### CONSEQUENCES

#### Hooks for subclasses

Creating objects inside a class with a factory method is always more flexible than creating an object directly. Factory
Method gives subclasses a hook for providing an extended version of an object.

#### Parallel class hierarchies connection

Exactly like in our example, there may be parallel class hierarchies, as a result of a class delegating some of its 
responsibilities to a separate class.