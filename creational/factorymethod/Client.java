package factorymethod;

public class Client {
    public static void main(String[] args) {
        Logistics logistics = new RoadLogistics();
        logistics.planDelivery(); // This will create a Truck and call its deliver method.

        logistics = new SeaLogistics();
        logistics.planDelivery(); // This will create a Ship and call its deliver method.
    }
}
