package builder;

interface CarBuilder {

    CarBuilder setBrand(String brand);

    CarBuilder setSpecification(String specification);

    CarBuilder setLocation(String location);

    Car build();
}

