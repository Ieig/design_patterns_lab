package builder;

class CarDirector {
    private final CarBuilder carBuilder;

    public CarDirector(CarBuilder carBuilder) {
        this.carBuilder = carBuilder;
    }

    public void constructAudiCar() {
        carBuilder.setBrand("AUDI").setSpecification("S-line").setLocation("Ingolstadt");
    }

    public void constructBmwCar() {
        carBuilder.setBrand("BMW").setSpecification("M package").setLocation("Munich");
    }
}
