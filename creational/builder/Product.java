package builder;

class Car {

    private final String brand;
    private final String specification;
    private final String location;

    public Car(String brand, String specification, String location) {
        this.brand = brand;
        this.specification = specification;
        this.location = location;
    }

    @Override
    public String toString() {
        return brand + " " + specification + " from " + location;
    }
}
