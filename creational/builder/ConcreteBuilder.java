package builder;

class ConcreteCarBuilder implements CarBuilder {

    private String brand;
    private String specification;
    private String location;

    @Override
    public CarBuilder setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    @Override
    public CarBuilder setSpecification(String specification) {
        this.specification = specification;
        return this;
    }

    @Override
    public CarBuilder setLocation(String location) {
        this.location = location;
        return this;
    }

    @Override
    public Car build() {
        return new Car(brand, specification, location);
    }
}