package builder;

public class Client {
    public static void main(String[] args) {
        CarBuilder builder = new ConcreteCarBuilder();
        CarDirector director = new CarDirector(builder);

        director.constructAudiCar();
        Car audi = builder.build();
        System.out.println(audi);

        director.constructBmwCar();
        Car bmw = builder.build();
        System.out.println(bmw);
    }
}
