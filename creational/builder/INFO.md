## Builder Pattern


### INTENT

Separate the construction of a complex object from its representation, so that the
same construction process can create different representations. 


### APPLICABILITY

- The algorithm for creating a complex object should be independent of the parts that make up the object and how 
they're assembled.
- The construction process must allow different representations for the object that's constructed.


### PARTICIPANTS

#### Builder:
- specifies an abstract interface for creating parts of a Product object.

#### ConcreteBuilder:
- constructs and assembles parts of the product by implementing theBuilder interface.
- defines and keeps track of the representation it creates.
- provides an interface for retrieving the product.

#### Director:
- constructs an object using the Builder interface.

#### Product:
- represents the complex object under construction. ConcreteBuilder builds the product's internal representation and 
defines the process by which it's assembled.
- includes classes that define the constituent parts, including interfaces for assembling the parts into the final result.


### EXAMPLE SCENARIO: _CARS_

Suppose we have a `Car` class, and we want to create different types of cars (like Audi and BMW) with various
features. We'll use the Builder pattern to construct these cars.

In this example, the `Car` is the Product, `CarBuilder` is the Builder interface, and `ConcreteCarBuilder` is the
ConcreteBuilder. `CarDirector` takes a `CarBuilder` object and defines methods like `constructAudiCar` and
`constructBmwCar` to encapsulate the construction of specific types of cars. The client code just needs to interact 
with the Director, making the process of creating different car types more streamlined and organized.


### CONSEQUENCES

#### Varied internal representation

The Builder object provides the director with an abstract interface for constructing the product. The interface lets the 
builder hide the representation and internal structure of the product. It also hides how the product gets assembled. 
Because the product is constructed through an abstract interface, all you have to do to change the product's internal 
representation is define a new kind of builder.

#### Isolation of code for construction and representation

The Builder pattern improves modularity by encapsulating the way a complex object is constructed and represented. 
Clients needn't know anything about the classes that define the product's internal structure; such classes don't appear 
in Builder's interface. Each ConcreteBuilder contains all the code to create and assemble a particular kind of product. 
The code is written once; then different Directors can reuse it to build Product variants from the same set of parts.

#### Finer control over the construction process

Unlike creational patterns that construct products in one shot, the Builder pattern constructs the product step by step 
under the director's control. Only when the product is finished does the director retrieve it from the builder. Hence 
the Builder interface reflects the process of constructing the product more than other creational patterns. This gives 
you finer control over the construction process and consequently the internal structure of the resulting product.
