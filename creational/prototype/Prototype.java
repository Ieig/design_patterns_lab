package prototype;

interface Shape {
    Shape getClone();
}
