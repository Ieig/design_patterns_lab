## Prototype Pattern


### INTENT

Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.


### APPLICABILITY

- System should be independent of how its products are created, composed, and represented.
- Classes to instantiate are specified at run-time, for example, by dynamic loading.
- To avoid building the class hierarchy of factories that parallels the class hierarchy of products.
- Instances of a class can have one of only a few different combinations of state. It may be more convenient to 
install a corresponding number of prototypes and clone them rather than instantiating the class manually, each time 
with the appropriate state.


### PARTICIPANTS

#### Prototype
- declares an interface for cloning itself.

#### ConcretePrototype
- implements an operation for cloning itself.

#### Client
- creates a new object by asking a prototype to clone itself


### EXAMPLE SCENARIO: _SHAPES_

Consider a simple scenario where we have different types of shapes that need to be duplicated. We will define a `Shape`
interface as Prototype and several concrete shapes as ConcretePrototypes that implement this interface.

- The `Shape` interface defines a `getClone` method.
- `Circle` and `Rectangle` are concrete implementations of `Shape`. 
Each class has a copy constructor and overrides the `getClone` method.
- In the `Client` class, we create instances of `Circle` and `Rectangle` and then clone these instances.


### CONSEQUENCES

#### Dynamic Configuration

In applications where the dynamic loading and instantiation of classes are needed. Objects can be configured dynamically
at runtime through cloning, which can be more flexible than static configuration at compile-time.

#### Performance

In some cases, cloning an existing object can be more efficient than creating a new instance, particularly if the object
creation is a heavy operation.

#### Reduced subclassing

Prototype can reduce the need for creating subclasses. It's particularly useful when object construction requires a lot
of steps and the number of configurations is high. FactoryMethod often produces a hierarchy of Creator classes that 
parallels the product class hierarchy. The Prototype pattern lets you clone a prototype instead of asking a factory 
method to make a new object. Hence, you don't need a Creator class hierarchy at all.