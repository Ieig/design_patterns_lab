package prototype;

public class Client {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Circle anotherCircle = (Circle) circle.getClone();
        System.out.println(circle);
        System.out.println(anotherCircle);

        Rectangle rectangle = new Rectangle(10, 20);
        Rectangle anotherRectangle = (Rectangle) rectangle.getClone();
        System.out.println(rectangle);
        System.out.println(anotherRectangle);
    }
}
