package prototype;

import java.util.UUID;

class Circle implements Shape {
    private final int radius;
    private final UUID uuid;

    public Circle(int radius) {
        this.radius = radius;
        this.uuid = UUID.randomUUID();
    }

    public Circle(Circle source) {
        this.radius = source.radius;
        this.uuid = UUID.randomUUID();
    }

    @Override
    public Shape getClone() {
        return new Circle(this);
    }

    @Override
    public String toString() {
        return "Circle with radius: " + radius + " and UUID: " + uuid;
    }
}

class Rectangle implements Shape {
    private final int width;
    private final int height;
    private final UUID uuid;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
        this.uuid = UUID.randomUUID();
    }

    public Rectangle(Rectangle source) {
        this.width = source.width;
        this.height = source.height;
        this.uuid = UUID.randomUUID();
    }

    @Override
    public Shape getClone() {
        return new Rectangle(this);
    }

    @Override
    public String toString() {
        return "Rectangle with width: " + width + " and height: " + height + " and UUID: " + uuid;
    }
}
