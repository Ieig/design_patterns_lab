## Abstract Factory Pattern


### INTENT

Provide an interface for creating families of related or dependent objects without specifying their concrete classes.


### APPLICABILITY

- A system should be independent of how its products are created, composed, and represented.
- A system should be configured with one of multiple families of products.
- A family of related product objects is designed to be used together, and you need to enforce this constraint.
- Want to provide a class library of products, and you want to reveal just their interfaces, not their implementations.


### PARTICIPANTS

#### AbstractFactory
- declares an interface for operations that create abstract product objects.

#### ConcreteFactory
- implements the operations to create concrete product objects.

#### AbstractProduct
- declares an interface for a type of product object.

#### ConcreteProduct
- defines a product object to be created by the corresponding concrete factory.
- implements theAbstractProduct interface.

#### Client 
- uses only interfaces declared by AbstractFactory and AbstractProduct classes.


### EXAMPLE SCENARIO: _FURNITURE FACTORY_

Imagine we have a furniture factory that needs to produce furniture in different styles, such as Modern and Victorian. 
Each style includes different types of furniture like `Chair`, `Sofa`, and `CoffeeTable`. 
We'll use the Abstract Factory pattern to create these different families of furniture items.

In this example, the `FurnitureFactory` interface acts as the abstract factory, defining a set of methods for creating 
various furniture items. The `ModernFurnitureFactory` and `VictorianFurnitureFactory` are concrete factories that 
implement these methods to create specific products. The client code works with the abstract `FurnitureFactory`interface, 
allowing it to use different factories to create different styles of furniture without being tied to concrete classes.


### CONSEQUENCES

#### Isolation and exchange between product families

The Abstract Factory pattern helps you control the classes of objects that an application creates. Because a factory 
encapsulates the responsibility and the process of creating product objects,it isolates clients from implementation 
classes. Clients manipulate instances through their abstract interfaces. Product class names are isolated in the 
implementation of the concrete factory;they do not appear in client code

#### Consistency among products

When product objects in a family are designed to work together,it's important that an application use objects from
only one family at a time. AbstractFactory makes this easy to enforce.

#### Complications with supporting new kinds of products

Extending abstract factories to produce new kinds of Products isn't easy.That's because the AbstractFactory interface 
fixes the set of products that can be created.Supporting new kinds of products requires extending the factory interface, 
which involves changing the AbstractFactory class and all of its subclasses.