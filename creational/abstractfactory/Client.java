package abstractfactory;

public class Client {
    public static void main(String[] args) {
        FurnitureFactory factory = new ModernFurnitureFactory();
        Chair chair = factory.createChair();
        Sofa sofa = factory.createSofa();
        CoffeeTable coffeeTable = factory.createCoffeeTable();

        System.out.println("Furniture Style: " + chair.getStyle());
        chair.sitOn();
        sofa.relaxOn();
        coffeeTable.placeItems();

        factory = new VictorianFurnitureFactory();
        chair = factory.createChair();
        sofa = factory.createSofa();
        coffeeTable = factory.createCoffeeTable();

        System.out.println("Furniture Style: " + chair.getStyle());
        chair.sitOn();
        sofa.relaxOn();
        coffeeTable.placeItems();
    }
}
