package abstractfactory;

class ModernChair implements Chair {

    @Override
    public void sitOn() {
        System.out.println("Sitting on a modern chair.");
    }

    @Override
    public String getStyle() {
        return "Modern";
    }
}

class VictorianChair implements Chair {

    @Override
    public void sitOn() {
        System.out.println("Sitting on a Victorian chair.");
    }

    @Override
    public String getStyle() {
        return "Victorian";
    }
}


class ModernSofa implements Sofa {

    @Override
    public void relaxOn() {
        System.out.println("Relaxing on a modern sofa.");
    }

    @Override
    public String getStyle() {
        return "Modern";
    }
}

class VictorianSofa implements Sofa {

    @Override
    public void relaxOn() {
        System.out.println("Relaxing on a Victorian sofa.");
    }

    @Override
    public String getStyle() {
        return "Victorian";
    }
}


class ModernCoffeeTable implements CoffeeTable {

    @Override
    public void placeItems() {
        System.out.println("Placing items on a modern coffee table.");
    }

    @Override
    public String getStyle() {
        return "Modern";
    }
}

class VictorianCoffeeTable implements CoffeeTable {

    @Override
    public void placeItems() {
        System.out.println("Placing items on a Victorian coffee table.");
    }

    @Override
    public String getStyle() {
        return "Victorian";
    }
}