package abstractfactory;

interface Chair {

    void sitOn();

    String getStyle();
}

interface Sofa {

    void relaxOn();

    String getStyle();
}

interface CoffeeTable {

    void placeItems();

    String getStyle();
}